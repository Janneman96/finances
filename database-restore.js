import paymentRepository from './repositories/paymentRepository.js'

window.fillData = function () {
  localStorage.setItem('finances', categories);
  for (var i = 0; i < JSON.parse(payments).length; i++) {
    var row = JSON.parse(payments)[i]

    var category = parseInt(row['category'])

    for (var item in row) {
      if (item.split('-').length == 2) {
        console.log(row[item])
        var year = item.split('-')[0]
        var month = item.split('-')[1]
        var amount = row[item]
        paymentRepository.add(category, amount, month, year)
      }
      location.reload();
    }
  }
}