var STORAGE = 'finances'

var repository = {
  _getStorage() {
    var storage = JSON.parse(localStorage.getItem(STORAGE))
    if (!storage) {
      this._setStorage({})
      return this._getStorage()
    }
    return storage
  },
  _setStorage(storage) {
    localStorage.setItem(STORAGE, JSON.stringify(storage))
  },

  /**
   * 
   * @param {string} name The name of the repository in the localstorage
   * 
   * @returns {{name: string, highestId: number, list: Array}}
   */
  getRepository(name) {
    var repository = this._getStorage()[name]

    if (!repository) {
      var storage = this._getStorage()
      storage[name] = {
        name: name,
        highestId: -1,
        list: []
      }
      this._setStorage(storage)
      return this.getRepository(name)
    }

    return this.decorateWithFunctions(repository)
  },
  /**
   * Adds all functionality to the repository object
   * @param {any} repository Repository object without functions
   * 
   * @returns {any} Repository object with functions
   */
  decorateWithFunctions(repository) {
    repository.getAll = () => {
      return this.getRepository(repository.name).list
    }
    repository.saveChanges = (updatedRepository) => {
      var storage = this._getStorage()
      storage[updatedRepository.name] = {
        name: updatedRepository.name,
        highestId: updatedRepository.highestId,
        list: updatedRepository.list
      }
      this._setStorage(storage)
    }
    /**
     * Adds an item to the storage
     */
    repository.add = function (item, overwriteId) {
      var targetId
      if (overwriteId) {
        targetId = overwriteId
      } else {
        this.highestId++
        targetId = this.highestId
        item.id = targetId
      }
      this.list[targetId] = item
      this.saveChanges(this)
    }
    return repository
  }
}

export default repository