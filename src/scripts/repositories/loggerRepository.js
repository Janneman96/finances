import repository from './repository.js'

var LOGGER_STORAGE = 'logger'

var _repository = repository.getRepository(LOGGER_STORAGE)

var loggerRepository = {
  /**
   * @returns {Array} All categories
   */
  getAll() {
    return this.withCategory(_repository.getAll())
  },

  add(id, message, time) {
    _repository.add(performanceLog.getData(), false)
  }
}

export default loggerRepository