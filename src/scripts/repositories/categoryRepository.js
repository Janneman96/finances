import repository from './repository.js'
import categoryFactory from '../factories/categoryFactory.js'

var CATEGORY_STORAGE = 'categories'

var _repository = repository.getRepository(CATEGORY_STORAGE)

var categoryRepository = {
  /**
   * @returns {Array} All payments
   */
  getAll() {
    return _repository.getAll()
  },

  /**
   * Adds a category to the storage
   * @param {string} name Category display name
   * @param {string} color Hex color
   * @param {boolean} isIncome income = true, expense = false
   */
  add(name, color, isIncome) {
    var category = categoryFactory.create(name, color, isIncome)
    var errorMessage = category.getValidationErrors()
    if (errorMessage.length) return errorMessage

    _repository.add(
      category.getData(),
      this.getIndex(name, isIncome))
  },

  /**
   * 
   * @param {string} name Category display name
   * @param {boolean} isIncome Income = true, expense = false
   * 
   * @returns {void,number} Returns the index of a category or void if it does not exist
   */
  getIndex(name, isIncome) {
    var categories = this.getAll()
    for (var index in categories)
      if (categories[index].name == name && categories[index].isIncome == isIncome) return index
  }
}

export default categoryRepository