import repository from './repository.js'

var TEST_REPOSITORY_NAME = 'test'

var testRunner = {
  /**
   * Displays a success message in the console
   * @param {string} message Message to display
   * 
   * @returns {void}
   */
  success(message) {
    console.log('%cTest succes: ' + message, 'color: green;')
  },
  /**
   * Displays an error message in the console
   * @param {string} name Test function name
   * @param {string} reason Reason of the test failing
   * 
   * @returns {void}
   */
  error(name, reason) {
    console.error('Test failed: ' + name, reason)
  },
  /**
   * Runs all tests
   */
  runAll() {
    console.log('Running all tests...')
    var succeeded = 0
    var failed = 0
    for (var property in tests) {
      var testGroup = tests[property]
      console.log('%c' + property, 'font-weight: bold; font-size: 16px;')
      for (var property in testGroup) {
        var response = testGroup[property]()
        if (response) {
          this.error(property, response)
          failed++
        } else {
          this.success(property)
          succeeded++
        }
      }
    }

    if (!failed) {
      console.log('%cAll ' + succeeded + ' tests have succeeded', 'color: green; font-weight: bold; font-size:16px;')
    } else {
      console.error('%c' + failed + ' tests have failed', 'font-size: 16px;')
    }
  }
}

var tests = {
  repositoryTests: {
    resetTestRepository() {
      var storage = repository._getStorage()
      delete storage[TEST_REPOSITORY_NAME]
      repository._setStorage(storage)
    },
    doesGetRepository() {
      var testRepository = repository.getRepository(TEST_REPOSITORY_NAME)
      if (!testRepository) return 'repository is null'
      if (!testRepository.name) return 'name was not set'
      if (testRepository.highestId != -1) return 'highestId is not -1'
      if (!testRepository.list) return 'list was not initialised'
    },
    doesHaveCorrectFunctions() {
      var testRepository = repository.getRepository(TEST_REPOSITORY_NAME)
      if (!testRepository.getAll) return 'no getAll function'
      if (!testRepository.add) return 'no add function'
    },
    canGetAll() {
      var testRepository = repository.getRepository(TEST_REPOSITORY_NAME)
      if (!testRepository.getAll()) return 'returns null'
      if (testRepository.getAll().length !== 0) return 'array is not empty'
    },
    addedItemsShowsUpInList() {
      var testRepository = repository.getRepository(TEST_REPOSITORY_NAME)
      testRepository.add({ name: 'testItem0' })
      testRepository.add({ name: 'testItem1' })
      var result = testRepository.getAll()
      if (testRepository.highestId !== 1) return 'highest id was not updated properly'
      if (!result[0]) return 'first item was not added'
      if (!result[1]) return 'second item was not added'
      if (result[0].name != 'testItem0') return 'first item does not have the correct name'
      if (result[1].name != 'testItem1') return 'second item does not have the correct name'
    },
  }
}

export default testRunner