import repository from '../repositories/repository.js'
import categoryRepository from '../repositories/categoryRepository.js'
import paymentFactory from '../factories/paymentFactory.js'

var PAYMENT_STORAGE = 'payments'

var _repository = repository.getRepository(PAYMENT_STORAGE)

var paymentRepository = {
  /**
   * @returns {Array} All payments with their category attached
   */
  getAll() {
    return this.withCategory(_repository.getAll())
  },

  /**
   * Replaces the category index/id with the correct category object
   * @param {Array} payments Payment list with category index as category property
   * 
   * @returns {Array} Payment list with category object as category property
   */
  withCategory(payments) {
    var categories = categoryRepository.getAll()
    for (var i = 0; i < payments.length; i++) {
      payments[i].category = categories[payments[i].category]
    }
    return payments
  },

  /**
   * 
   * @param {number} category Category index/id
   * @param {number} amount Amount in cents
   * @param {number} month Moment of payment - month (1 is January)
   * @param {number} year Moment of payment - year
   * 
   * @returns {void}
   */
  add(category, amount, month, year) {
    var payment = paymentFactory.create(category, amount, month, year)
    console.log(payment.category)
    var errorMessage = payment.getValidationErrors()
    if (errorMessage) return errorMessage
    var paymentData = payment.getData()
    _repository.add(paymentData, false)
  },

  /**
   * @returns The amount of cents of the heighest pillar in the graph
   */
  getHighestPillarAmount(paymentList) {
    var amountsPerPillar = {};
    for (var i = 0; i < paymentList.length; i++) {
      var payment = paymentList[i]
      var property = payment.year + '-' + payment.month + '-' + payment.category.isIncome
      var amount = payment.amount

      var monthCumulitive = amountsPerPillar[property]
      if (!monthCumulitive) amountsPerPillar[property] = 0
      amountsPerPillar[property] += parseInt(amount)
    }

    var highestAmount = 0
    for (var month in amountsPerPillar) {
      if (amountsPerPillar[month] > highestAmount) highestAmount = amountsPerPillar[month]
    }

    return highestAmount
  }
}

export default paymentRepository