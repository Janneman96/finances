var paymentFactory = {
  /**
   * Creates a payment object
   * @param {number} category Category index in category list
   * @param {number} amount Amount of money in cents
   * @param {number} month Month number, january is 1
   * @param {number} year The year of the moment of paying
   * @param {number} created Moment of creation. Default is Date.now()
   * 
   * @returns {any} Payment object
   */
  create(...args) {
    return new Payment(...args)
  }
}

export default paymentFactory

class Payment {
  constructor(category, amount, month, year, created) {
    this.category = category
    this.amount = amount
    this.month = month
    this.year = year
    this.created = created ? created : Date.now()

    /**
     * Validates payment
     * @returns {string} Error message, empty string if no error
     */
    this.getValidationErrors = () => {
      var errorMessage = ''
      if (!this.year) errorMessage += 'Vul het jaar in. '
      if (!this.month) errorMessage += 'Kies een maand. '
      if (!this.category && this.category !== 0) errorMessage += 'Kies een categorie. '
      var isMoney = /(^\d*[\.\,]\d{0,2}$)|(^\d+$)/.test(this.amount)
      if (!isMoney) errorMessage += 'Vul een geldig bedrag in. '

      this.amount = this.format(this.amount)
      if (this.amount == '0.00') errorMessage += 'Het bedrag mag niet 0 zijn. '
      this.amount = this.formatToCents(this.amount)

      // if (this.category) this.category = this.category.value // TODO: change category.value to category.data()
      // if (this.month) this.month = this.month.value // TODO: change month.value to month.data()

      return errorMessage
    }

    /**
     * @returns Payment data object
     */
    this.getData = () => {
      var paymentToReturn = {
        category: this.category,
        amount: this.amount,
        month: this.month,
        year: this.year,
        created: this.created
      }
      return paymentToReturn
    }

    /**
     * Formats the amount in euro's to the "\d+\.\d\d" format
     * @param {string} amount Amount in euro's, seperated with a comma or dot
     */
    this.format = (amount) => {
      amount = amount.replace(',', '.')
      if (amount.includes('.')) {
        if (amount.split('.')[1].length === 1) amount += '0'
        if (amount.split('.')[1].length === 0) amount += '00'
        if (amount.split('.')[0].length === 0) amount = '0' + amount
      }
      return amount
    }

    /**
     * Change euro string to cents string
     * @param {string} amount Amount in cents
     */
    this.formatToCents = (amount) => {
      if (amount.includes('.')) {
        var splittedAmount = amount.split('.')
        return splittedAmount[0] + splittedAmount[1]
      }
      return amount + '00'
    }
  }
}