var performanceLogFactory = {
  create(id, message) {
    return {
      id: id,
      message: message,

      getData() {
        return {
          id: this.id,
          message: this.message,
        }
      }
    }
  }
}

export default performanceLogFactory