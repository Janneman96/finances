var categoryFactory = {
  /**
   * Creates a category object
   * @param {string} name Display name
   * @param {string} color Hex string
   * @param {boolean} isIncome Income = true, expense = false
   * 
   * @returns Category object
   */
  create(name, color, isIncome) {
    return {
      name: name,
      color: color,
      isIncome: isIncome,

      /**
       * Validates category
       * @returns {string} Error message, empty string if no error
       */
      getValidationErrors() {
        var errorMessage = ''
        if (!this.name) errorMessage += 'Vul de naam in. '
        var isHexColor = /^#([0-9a-fA-F]{3}){1,2}$/.test(this.color)
        if (!isHexColor) errorMessage += 'Vul een geldige kleur in. '
        return errorMessage
      },

      /**
       * @returns {name: string, color: string, isIncome: boolean} Category data object
       */
      getData() {
        return {
          name: this.name,
          color: this.color,
          isIncome: this.isIncome
        }
      }
    }
  }
}

/**
 * The categoryFactory is responsible for defining what properties and functions a category has.
 */
export default categoryFactory