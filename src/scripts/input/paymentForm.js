import paymentRepository from '../repositories/paymentRepository.js'
import userEfficiency from './userEfficiency.js'
import elementHelper from '../helpers/elementHelper.js'
import monthPillars from '../graphs/monthPillars.js'

var paymentForm = {
  /**
   * Initializes the payment form
   */
  init() {
    var inputAmount = userEfficiency.getInputAmount()

    function submitPayment() {
      var category = document.querySelector('input[name="category"]:checked').value
      var amount = inputAmount.value
      var month = document.querySelector('input[name="month"]:checked').value
      var year = document.querySelector('.js-payment-year').value
      var errorMessage = paymentRepository.add(category, amount, month, year)
      if (errorMessage) {
        notifyInvalid(errorMessage)
      } else {
        inputAmount.value = ""
        displayLatestPayments()
        monthPillars.init()
      }
    }
    elementHelper.runOnAll('.js-submit-payment', function (submitPaymentButton) {
      submitPaymentButton.addEventListener('click', submitPayment)
    })

    function notifyInvalid(errorMessage) {
      alert(errorMessage)
    }

    function displayLatestPayments() {
      var payments = paymentRepository.getAll()
      elementHelper.runOnAll('.js-latest-payments', function (recentPayments) {
        recentPayments.innerHTML = '<p>Recente invoer:</p>'
        if (payments) {
          for (var i = payments.length - 1; i >= 0; i--) {
            var payment = payments[i]
            if (i < payments.length - 10) {
              recentPayments.innerHTML += '<br/>en nog ' + (payments.length - 10) + '...'
              return
            }
            var category = (payment.category.isIncome ? 'in-' : 'out-') + payment.category.name
            recentPayments.innerHTML += category + ': ' +
              payment.amount.substring(0, payment.amount.length - 2) + '.' + payment.amount.substring(payment.amount.length - 2, payment.amount.length) + ' euro, ' +
              'maand ' + payment.month + ', ' + payment.year + '. Ingevoerd op ' +
              new Date(payment.created).toLocaleString() + '<br/>'
          }
        }
      })
    }

    displayLatestPayments()
  }
}
export default paymentForm