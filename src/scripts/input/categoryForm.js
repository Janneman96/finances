import categoryRepository from '../repositories/categoryRepository.js'
import elementHelper from '../helpers/elementHelper.js'
import monthPillars from '../graphs/monthPillars.js'

var categoryForm = {
  /**
   * Initializes the category form
   */
  init() {
    function submitCategory() {
      var name = document.querySelector('input[name="name"]').value
      var color = document.querySelector('input[name="color"]').value
      var isIncome = document.querySelector('input[name="is-income"]:checked').value == 'true'
      var errorMessage = categoryRepository.add(name, color, isIncome)
      if (errorMessage) notifyInvalid(errorMessage)
      else {
        displayCategories()
        monthPillars.init()
      }
    }

    function notifyInvalid(errorMessage) {
      alert(errorMessage)
    }

    elementHelper.runOnAll('.js-submit-category', function (element) {
      element.addEventListener('click', submitCategory)
    })

    function displayCategories() {
      var categories = splitIncomeAndExpenses()
      elementHelper.runOnAll('.js-categories-income', function (income) {
        addCategoryElements(income, categories.income)
      })

      elementHelper.runOnAll('.js-categories-expenses', function (expenses) {
        addCategoryElements(expenses, categories.expenses)
      })
    }

    function splitIncomeAndExpenses() {
      var incomeCategories = []
      var expensesCategories = []
      var allCategories = categoryRepository.getAll()
      for (var i = 0; i < allCategories.length; i++) {
        var category = allCategories[i]
        if (category.id) {
          if (category.isIncome) incomeCategories.push(category)
          else expensesCategories.push(category)
        }
      }
      return { income: incomeCategories, expenses: expensesCategories }
    }

    function addCategoryElements(parent, categories) {
      parent.innerHTML = ''
      for (var i = 0; i < categories.length; i++) {
        var category = categories[i]
        var element = "<label style='background-color:" + category.color + "' class='js-focus-amount-after-click'><input type='radio' name='category' value='" + category.id + "'>" + category.name + "</label>"
        parent.innerHTML += element
      }
    }

    displayCategories()
  }
}

export default categoryForm