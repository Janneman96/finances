import elementHelper from '../helpers/elementHelper.js'

var userEfficency = {
  /**
   * Initializes user efficiënty javascript and returns the input element for the amount of a payment
   */
  getInputAmount() {
    elementHelper.runOnAll('.js-payment-year', function (inputYear) {
      inputYear.value = new Date(Date.now()).getFullYear()
    })

    var inputAmount = document.querySelector('.js-payment-amount')

    elementHelper.runOnAll('fieldset label, .js-categories-income, .js-categories-expenses', function (element) {
      element.addEventListener('click', function (event) {
        if (event.target.nodeName == 'INPUT') inputAmount.focus()
      })
    })

    inputAmount.addEventListener('keyup', function (event) {
      if (event.keyCode === 13) {
        elementHelper.runOnAll('.js-submit-payment', function (submitPaymentButton) {
          submitPaymentButton.click()
        })
      }
    })
    return inputAmount
  }
}

export default userEfficency