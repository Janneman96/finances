import paymentRepository from '../repositories/paymentRepository.js'
import elementHelper from '../helpers/elementHelper.js'

var monthPillars = {
  /**
   * Initialize the monthpillars graph
   * 
   * @returns {void}
   */
  init() {
    this.render()
  },

  /**
   * Checks if the montPillar already exists in the array
   * @param {any} monthPillar A monthpillar
   * @param {Array} monthPillars Target monthpillar array
   */
  contains(monthPillar, monthPillars) {
    for (var i = 0; i < monthPillars.length; i++) {
      if (monthPillars[i] == monthPillar) return true
    }
  },

  /**
   * Renders all payment blocks with the correct height and colors
   * @returns {void}
   */
  render() {
    var allPayments = paymentRepository.getAll()
    //TODO: sort payments properly
    allPayments.sort(function (left, right) {
      if (left.year < right.year || left.year == right.year && parseInt(left.month) < parseInt(right.month)) return -1
      else return 0
    })

    var highestPillarAmount = paymentRepository.getHighestPillarAmount(allPayments)

    var monthPillars = []
    for (var i = 0; i < allPayments.length; i++) {
      var payment = allPayments[i]
      var monthPillar = payment.year + '-' + payment.month
      this.add(monthPillar, monthPillars)
    }

    var months = ['', 'januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december']
    elementHelper.runOnAll('.js-graph', (element) => {
      var paymentPointer = 0
      element.innerHTML = ''
      var newInnerHtml = ''
      for (var i = 0; i < monthPillars.length; i++) {
        var monthPillar = monthPillars[i]
        var income = ''
        var expenses = ''
        while (paymentPointer < allPayments.length) {
          var payment = allPayments[paymentPointer]
          if (payment.year + '-' + payment.month != monthPillar) break
          if (payment.category.isIncome) income += this.renderBlock(payment, highestPillarAmount)
          else expenses += this.renderBlock(payment, highestPillarAmount);
          paymentPointer++
        }
        newInnerHtml += "<div class='month js-pillar js-pillar-" + monthPillar + "'>"
        newInnerHtml += "<div class='js-in'>"
        newInnerHtml += income
        newInnerHtml += "</div>"
        newInnerHtml += "<div class='js-out'>"
        newInnerHtml += expenses
        newInnerHtml += "</div>"
        newInnerHtml += "<div class='month-display-name'>"
        newInnerHtml += months[monthPillar.split('-')[1]] + " " + monthPillar.split('-')[0]
        newInnerHtml += "</div>"
        newInnerHtml += "</div>"
      }
      element.innerHTML = newInnerHtml
    })
  },

  /**
   * Add a new monthpillar to an existing monthpillar array
   * @param {any} monthPillar New monthpillar
   * @param {Array} monthPillars Existing monthpillar array
   */
  add(monthPillar, monthPillars) {
    if (!this.contains(monthPillar, monthPillars))
      monthPillars.push(monthPillar)
  },

  /**
   * Renders a payment block to an html string
   * @param {any} payment Payment with category object
   * 
   * @returns {string} The html for a single graph block
   */
  renderBlock(payment, highestPillarAmount) {    
    var block = '';
    block += "<div class='" + payment.category.name + "'"
    block += "style='"
    block += 'height: ' + this.calculateHeightInPixels(payment.amount, highestPillarAmount) + '%;'
    block += 'background-color: ' + payment.category.color
    block += "'"
    block += '></div>'
    return block
  },

  /**
   * @returns {number} Integer value of the amount of pixels to display as the height, calculated with +0.5
   */
  calculateHeightInPixels(amount, highestPillarAmount) {
    var roundedPercentage = (amount * 100) / highestPillarAmount
    return roundedPercentage
  }
}

export default monthPillars