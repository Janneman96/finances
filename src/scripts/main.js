import paymentForm from './input/paymentForm.js'
import categoryForm from './input/categoryForm.js'
import monthPillars from './graphs/monthPillars.js'
// import testRunner from './repositories/repository.test.js'

paymentForm.init()
categoryForm.init()
monthPillars.init()
// testRunner.runAll()
