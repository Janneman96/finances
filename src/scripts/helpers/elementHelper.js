var elementHelper = {
  /**
   * Run an action on all elements that match the selector query
   * @param {string} selector Querystring selector
   * @param {Function} action Function with an html element as its only parameter
   */
  runOnAll(selector, action) {
    var allElements = document.querySelectorAll(selector)
    for (var i = 0; i < allElements.length; i++) action(allElements[i])
  }
}

export default elementHelper