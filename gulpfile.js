'use strict'

var gulp = require('gulp'),
  sass = require('gulp-sass'),
  runSequence = require('run-sequence'),
  browserSync = require('browser-sync').create(),
  uglify = require('gulp-uglify')

gulp.task('build-html', function () {
  return gulp.src('./src/*.html')
    .pipe(gulp.dest('./build'))
    .pipe(browserSync.reload({
      stream: true
    }))
})

gulp.task('build-sass', function () {
  return gulp.src('./src/styling/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./build/styling'))
    .pipe(browserSync.reload({
      stream: true
    }))
})

gulp.task('build-js', function () {
  return gulp.src('./src/scripts/**/*.js')
    .pipe(gulp.dest('./build/scripts'))
    .pipe(browserSync.reload({
      stream: true
    }))
})

gulp.task('browserSync', function () {
  browserSync.init({
    server: {
      baseDir: 'build'
    }
  })
})

gulp.task('watch', ['browserSync', 'build-sass', 'build-html', 'build-js'], function () {
  gulp.watch('./src/styling/**/*.scss', ['build-sass'])
  gulp.watch('./src/*.html', ['build-html'])
  gulp.watch('./src/scripts/**/*.js', ['build-js'])
})

gulp.task('default', runSequence('watch'))

// currently at https://css-tricks.com/gulp-for-beginners/#post-207154 (watch html and js)